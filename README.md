Program that takes an employees annual income and increases it based on how long the employee has worked for the company.
If the employee has worked for 10+ years their annual income increases by 10%.
If the employee has worked for less than 10 years their annual income increases by 7%.
If the employee has worked for less than 5 years, but more than 3 their annual income increases by 5%.
If the employee has worked for less than 3 years their annual income increases by 3%.

*Note this program was created in QT Creator in a Linux based system.