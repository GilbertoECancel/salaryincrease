//Gilberto E.Cancel Morales
//CCOM 3033-001

#include <iostream>

using namespace std;

int main()
{

double annual_income;
int years;


//Take annual income.
cout<< "What is yout annual income?"<< endl;
cin>>annual_income;


//Take years worked for.
cout<< "How long have you worked for? (IN WHOLE NUMBERS)"<< endl;
cin>> years;


    // Determine how much increment is to be given to the annual income.
    if(years >= 10)
        annual_income = annual_income * 1.1;

    else if (years < 10 && years > 5)
        annual_income = annual_income * 1.07;

    else if(years < 5 && years > 3)
        annual_income = annual_income * 1.05;

    else if(years < 3)
        annual_income = annual_income * 1.03;



cout<< "This should be your annual income "<< annual_income <<"$."<< endl;



    return 0;
}

